<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/about', function () {
    $name = "Razan Nadhif";
    return view('about', ['name' => $name]);
});

// Route::get('/contact','App\Http\Controllers\TestController@contact');

Route::get('/contact', function () {
    $number = ['razan@gmail.ub','185150700111030','UB'];
    return view('contact', ['number' => $number]);
});